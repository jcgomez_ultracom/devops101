import unittest
#import sys
#sys.path.append('${WORKSPACE}/DevOps101/py/s')

from processing import do_calculation

number1 = 10
number2 = 20
result = 0

result = do_calculation(number1,number2)

class TestSum(unittest.TestCase):

    def test_sum(self):
        self.assertTrue(result == 30)
        self.assertFalse(result != 30)

if __name__ == '__main__':
    unittest.main()