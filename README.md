https://gitlab.com/jcgomez_ultracom/devops101/
hola
Useful git commands:
- git state
- git diff
- git commit
- git push
- git pull

Useful docker commands:
- docker ps
- docker ps -a
- docker images
- docker build . -t TAG --build-arg ARGUMENT="ARGUMENT"
- docker run -d -p PortServer:PortContainer TAG

EXAMPLES:

docker build . -t webserver:v1 --build-arg USERNAME="git_user" --build-arg PASSWORD="git_user_passwd"

docker run -d -p 8080:80 webserver:v1

Useful jenkinsfile means:
- Pipeline: Building a jenkinsfile we can have metrics
- Stage: Each step you configure in the pipeline
- Steps: Each of the commands that will be executed
- Agent: Node where the pipeline instructions will be executedgi
